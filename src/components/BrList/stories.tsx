import { Story, Meta } from "@storybook/react";
import BrItem from "@/components/BrItem";

import BrList, { BrListProps } from ".";

const child = (
  <>
    <BrItem>Item 1</BrItem>
    <BrItem>Item 2</BrItem>
  </>
);

export default {
  title: "input/BrList",
  component: BrList,
  args: {
    title: "Lista de Itens",
    children: child,
  },
} as Meta;

export const Simples: Story<BrListProps> = (args) => <BrList {...args} />;
