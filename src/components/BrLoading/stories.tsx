import { Story, Meta } from "@storybook/react";

import BrLoading, { BrLoadingProps } from ".";

export default {
  title: "Feedback/BrLoading",
  component: BrLoading,
} as Meta;

const Template: Story<BrLoadingProps> = (args) => <BrLoading {...args} />;

export const Simples = Template.bind({});

export const Medium = Template.bind({});
Medium.args = { ...Medium.args, medium: true };

export const ComRotulo = Template.bind({});
ComRotulo.args = { ...ComRotulo.args, label: "Carregando..." };
