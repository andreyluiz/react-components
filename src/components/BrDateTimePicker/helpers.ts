import { DTPickerType, DateOptions, TimeOptions, DTPickerInput } from "./types";

type DTPickerOptions = {
  inputType: "text" | "time" | "datetime-local";
  placeholder: string;
  icon: string;
  defaultDate?: DTPickerInput;
  dateFormat?: string;
  ariaDateFormat?: string;
  noCalendar?: boolean;
  enableTime?: boolean;
};

export const getOptions = (
  type: DTPickerType,
  defaultDate?: DTPickerInput,
  dateProps?: DateOptions,
  timeProps?: TimeOptions
): DTPickerOptions & DateOptions & TimeOptions => {
  const options: Record<DTPickerType, DTPickerOptions> = {
    date: {
      inputType: "text",
      placeholder: "dd/mm/aaaa",
      icon: "calendar-alt",
      dateFormat: "d/m/Y",
      ariaDateFormat: "j de F de Y",
      defaultDate,
      ...dateProps,
    },
    time: {
      inputType: "time",
      placeholder: timeProps?.enableSeconds ? "hh:mm:ss" : "hh:mm",
      icon: "clock",
      noCalendar: true,
      enableTime: true,
      defaultDate,
      ...timeProps,
    },
    dateTime: {
      inputType: "datetime-local",
      placeholder: "dd/mm/aaaa hh:mm",
      icon: "calendar-alt",
      dateFormat: timeProps?.enableSeconds ? "d/m/Y H:i:S" : "d/m/Y H:i",
      ariaDateFormat: timeProps?.enableSeconds ? "j de F de Y H:i:S" : "j de F de Y H:i",
      enableTime: true,
      defaultDate,
      ...dateProps,
      ...timeProps,
    },
  };

  return options[type];
};
